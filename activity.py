from abc import ABC, abstractclassmethod

class Animal(ABC):
	# Using this means that the 'eat' and 'make_sound' functions have to be declared also in the child classes of this Animal class.
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound(self):
		pass

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# getters of Dog class
	def get_name(self):
		return self._name

	def get_breed(self):
		return self._breed

	def get_age(self):
		return self._age

	# setters of Dog class
	def set_name(self):
		self._name = name 

	def set_breed(self):
		self._breed = breed

	def set_age(self):
		self._age = age 

	# implementing the abstract methods from the parent class
	def eat(self, food):
		print(f"Eaten the {food}")

	def make_sound(self):
		print(f"Moo")

	def call(self):
		print(f"Here {self._name}")


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	# getters of Cat class
	def get_name(self):
		return self._name

	def get_breed(self):
		return self._breed

	def get_age(self):
		return self._age

	# setters of Cat class
	def set_name(self):
		self._name = name 

	def set_breed(self):
		self._breed = breed

	def set_age(self):
		self._age = age 

	# implementing the abstract methods from the parent class
	def eat(self, food):
		print(f"Eaten the {food}")

	def make_sound(self):
		print(f"Tweet")

	def call(self):
		print(f"Here {self._name}")

# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Cat food")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Dog food")
cat1.make_sound()
cat1.call()